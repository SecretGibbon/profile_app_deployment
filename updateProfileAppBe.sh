#!/bin/sh

cd ./profile_app_be
git pull
docker build -t secret_gibbon/profile_app_be -f /opt/docker/profile_app_be/Dockerfile /opt/docker/profile_app_be/.
docker-compose up --force-recreate -d profile_app_be


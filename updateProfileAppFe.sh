#!/bin/sh

cd ./profile_app_fe
git pull
docker build -t secret_gibbon/profile_app_fe -f /opt/docker/profile_app_fe/Dockerfile /opt/docker/profile_app_fe/.
docker-compose up --force-recreate -d profile_app_fe

